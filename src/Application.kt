package com.veno

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.http.ContentType
import io.ktor.http.cio.websocket.Frame
import io.ktor.http.cio.websocket.WebSocketSession
import io.ktor.http.cio.websocket.readText
import io.ktor.response.respondText
import io.ktor.routing.get
import io.ktor.routing.post
import io.ktor.routing.routing
import io.ktor.websocket.webSocket
import kotlinx.coroutines.delay
import kotlinx.coroutines.launch
import java.time.Duration
import java.util.*
import java.util.concurrent.ConcurrentHashMap

fun main(args: Array<String>): Unit = io.ktor.server.netty.EngineMain.main(args)


open class Task(private val uuid: String) {
    private val receivers = ConcurrentHashMap.newKeySet<WebSocketSession>()

    fun addReceiver(receiver: WebSocketSession) {
        receivers.add(receiver)
    }

    protected suspend fun sendToReceivers(frame: Frame) {
        receivers.forEach { it.send(frame) }
    }

    private suspend fun sendTaskIsStarting() {
        sendToReceivers(Frame.Text("Task $uuid is starting"))
    }

    private suspend fun sendTaskFailed() {
        sendToReceivers(Frame.Text("Task $uuid failed"))
    }

    private suspend fun sendTaskCompleted() {
        sendToReceivers(Frame.Text("completed:$uuid"))
    }

    suspend fun run() {
        sendTaskIsStarting()

        try {
            doRun()
        } catch (e: Exception) {
            sendTaskFailed()
            throw e
        }

        sendTaskFailed()
    }

    protected open suspend fun doRun() {
        delay(5000)
    }
}

class TaskWithParts(uuid: String) : Task(uuid) {
    private suspend fun sendPartialStatus(percentage: Int) {
        sendToReceivers(Frame.Text("status:$percentage"))
    }

    override suspend fun doRun() {
        delay(1000)
        sendPartialStatus(10)
        delay(4000)
        sendPartialStatus(40)
        delay(1000)
        sendPartialStatus(70)
        delay(1000)
        sendPartialStatus(90)
        delay(1000)
    }
}

@Suppress("unused") // Referenced in application.conf
@kotlin.jvm.JvmOverloads
fun Application.module(testing: Boolean = false) {
    install(io.ktor.websocket.WebSockets) {
        pingPeriod = Duration.ofSeconds(15)
        timeout = Duration.ofSeconds(15)
        maxFrameSize = Long.MAX_VALUE
        masking = false
    }

    val tasksStore = ConcurrentHashMap<String, Task>()

    routing {
        get("/") {
            call.respondText("HELLO WORLD!", contentType = ContentType.Text.Plain)
        }

        post("/api/sync-action") {
            delay(5000)
            call.respondText { "OK" }
        }

        post("/api/async-action") {
            val uuid = UUID.randomUUID().toString()
            val task = Task(uuid)
            tasksStore[uuid] = task
            call.respondText { uuid }
            launch {
                task.run()
                tasksStore.remove(uuid)
            }
        }

        post("/api/async-action-with-percentage") {
            val uuid = UUID.randomUUID().toString()
            val task = TaskWithParts(uuid)
            tasksStore[uuid] = task
            call.respondText { uuid }
            launch {
                task.run()
                tasksStore.remove(uuid)
            }
        }

        webSocket("/ws") {
            send(Frame.Text("CONNECTED"))
            while (true) {
                val frame = incoming.receive()
                if (frame is Frame.Text) {
                    val data = frame.readText()
                    if (data.startsWith("subscribe:")) {
                        val uuid = data.substring("subscribe:".length)
                        val task = tasksStore.get(uuid)
                        if (task != null) {
                            task.addReceiver(this)
                        } else {
                            send(Frame.Text("No such task"))
                        }
                    }
                }
            }
        }
    }
}

